<?php
/**
 * Template name: Plantilla Login
 */?>
<?php 
		 if($_SERVER['REQUEST_METHOD'] == 'POST'){
		 	//Obtener datos
		 		$creds = array();
				$creds['user_login'] = $_POST['user_login'];
				$creds['user_password'] = $_POST['user_password'];
				$creds['remember'] = false;
		 		//Comprobamos si los datos son correcto
		 		$user = wp_signon( $creds, false );
		 		
		 		if ( is_wp_error($user) ){
					$mensaje = $user->get_error_message();}
				else{
					header('Location:'. home_url('/'));
				}
		 } ?>

<!DOCTYPE html>
<html lang="es">
<head>

	<!-- start: Meta -->
	<meta charset="utf-8">
	<title><?php echo the_title( ); ?></title>
	<meta name="description" content="Bootstrap Metro Dashboard">
	<meta name="author" content="Dennis Ji">
	<meta name="keyword" content="Metro, Metro UI, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
	<!-- end: Meta -->
	
	<!-- start: Mobile Specific -->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<!-- end: Mobile Specific -->
	
	<!-- start: CSS -->
	<link id="bootstrap-style" href="<?php echo get_stylesheet_directory_uri();?>/css/bootstrap.min.css" rel="stylesheet">
	<link href="<?php echo get_stylesheet_directory_uri();?>/css/bootstrap-responsive.min.css" rel="stylesheet">
	<link id="base-style" href="<?php echo get_stylesheet_directory_uri();?>/css/style.css" rel="stylesheet">
	<link id="base-style-responsive" href="<?php echo get_stylesheet_directory_uri();?>/css/style-responsive.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
	<!-- end: CSS -->
	

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<link id="ie-style" href="css/ie.css" rel="stylesheet">
	<![endif]-->
	
	<!--[if IE 9]>
		<link id="ie9style" href="css/ie9.css" rel="stylesheet">
	<![endif]-->
		
	<!-- start: Favicon -->
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri();?>/img/favicon.ico">
	<!-- end: Favicon -->
	
			<style type="text/css">
			body { background: url(<?php echo get_stylesheet_directory_uri();?>/img/bg-login.jpg) !important; }
		</style>
		
</head>

<body>
		<div class="container-fluid-full">
			<div class="row-fluid">
				
				<div class="row-fluid">

					<div class="login-box">
							<?php if($mensaje){ ?>
								<div class="alert alert-danger">
									<strong>Error!</strong> Usuario/Password incorrecto.
								</div>
							<?php	} ?>
						<div class="icons">
							<a href="<?php echo home_url('/');?>"><i class="halflings-icon home"></i></a>
						</div>
						<h2><?php  _e("Login to your account"); ?></h2>
						<form class="form-horizontal" action="" method="post">
							<fieldset>
								
								<div class="input-prepend" title="Username">
									<span class="add-on"><i class="halflings-icon user"></i></span>
									<input class="input-large span10" name="user_login" id="username" type="text" placeholder="<?php _e("type username"); ?>"/>
								</div>
								<div class="clearfix"></div>

								<div class="input-prepend" title="Password">
									<span class="add-on"><i class="halflings-icon lock"></i></span>
									<input class="input-large span10" name="user_password" id="password" type="password" placeholder="<?php _e("type password"); ?>"/>
								</div>
								<div class="clearfix"></div>
								

								<div class="button-login">	
									<button type="submit" class="btn btn-primary"><?php _e("Login"); ?></button>
								</div>
								<div class="clearfix"></div>
						</form>
						<hr>
						<h3><?php _e("Forgot password"); ?></h3>
						<p>
							<?php _e("No problem"); ?>, <a href="#"><?php _e("click here"); ?></a> <?php _e("to get a new password"); ?>.
						</p>	
					</div><!--/span-->
				</div><!--/row-->
				

			</div><!--/.fluid-container-->
		
		</div><!--/fluid-row-->
	
	<!-- start: JavaScript-->

		<script src="<?php echo get_stylesheet_directory_uri();?>/js/jquery-1.9.1.min.js"></script>
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/jquery-migrate-1.0.0.min.js"></script>
	
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/jquery-ui-1.10.0.custom.min.js"></script>
	
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/jquery.ui.touch-punch.js"></script>
	
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/modernizr.js"></script>
	
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/bootstrap.min.js"></script>
	
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/jquery.cookie.js"></script>
	
		<script src='<?php echo get_stylesheet_directory_uri();?>/js/fullcalendar.min.js'></script>
	
		<script src='<?php echo get_stylesheet_directory_uri();?>/js/jquery.dataTables.min.js'></script>

		<script src="<?php echo get_stylesheet_directory_uri();?>/js/excanvas.js"></script>
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/jquery.flot.js"></script>
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/jquery.flot.pie.js"></script>
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/jquery.flot.stack.js"></script>
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/jquery.flot.resize.min.js"></script>
		
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/jquery.chosen.min.js"></script>
	
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/jquery.uniform.min.js"></script>
		
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/jquery.cleditor.min.js"></script>
	
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/jquery.noty.js"></script>
	
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/jquery.elfinder.min.js"></script>
	
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/jquery.raty.min.js"></script>
	
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/jquery.iphone.toggle.js"></script>
	
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/jquery.uploadify-3.1.min.js"></script>
	
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/jquery.gritter.min.js"></script>
	
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/jquery.imagesloaded.js"></script>
	
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/jquery.masonry.min.js"></script>
	
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/jquery.knob.modified.js"></script>
	
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/jquery.sparkline.min.js"></script>
	
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/counter.js"></script>
	
		<script src="<?php echo get_stylesheet_directory_uri();?>/js/retina.js"></script>

		<script src="<?php echo get_stylesheet_directory_uri();?>/js/custom.js"></script>
	<!-- end: JavaScript-->
	
</body>
</html>
